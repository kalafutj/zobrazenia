#include "interpolator.h"
#include <iostream>


interpolator::interpolator(){}

interpolator::interpolator(QColor* A,QColor* C,double beg,double end)
{
	reset(A, C, beg, end);
}

void interpolator::reset(QColor* A, QColor* C, double beg, double end) {
	kr = (C->red() - A->red()) / (end - beg);
	kg = (C->green() - A->green()) / (end - beg);
	kb = (C->blue() - A->blue()) / (end - beg);
	r = A->red();
	g = A->green();
	b = A->blue();
	R = C->red();
	G = C->green();
	B = C->blue();
}

QColor interpolator::operator++(int a) {
	r += kr;
	g += kg;
	b += kb;
	if (kr<0 && r<R)r = R;
	if (kr > 0&&r>R)r = R;
	if (kg < 0&&g<G)g = G;
	if (kg > 0&&g>G)g = G;
	if (kb < 0&&b<B)b = B;
	if (kb> 0&&b>B)b = B;
	return(QColor(r, g, b));
}

interpolator::~interpolator()
{
}
