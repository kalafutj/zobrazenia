#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "","(*.vtk)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
	options();
	//premietni();
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 800);
}

void MyPainter::premietni() {
	paintWidget.rotuj(ui.spinBox->value(), ui.spinBox_2->value());
	if (ui.radioButton_2->isChecked()) paintWidget.stredove(ui.spinBox_3->value(), ui.checkBox_2->isChecked());
	else paintWidget.rovnobezne(ui.checkBox_2->isChecked());

}

void MyPainter::options() {
	paintWidget.nastav_koeficienty(ui.doubleSpinBox_3->value(), ui.doubleSpinBox->value(), ui.doubleSpinBox_2->value(), ui.spinBox_13->value());
	paintWidget.nastav_farbu(ui.spinBox_7->value(), ui.spinBox_8->value(), ui.spinBox_9->value(), ui.spinBox_10->value(), ui.spinBox_11->value(), ui.spinBox_12->value());
	paintWidget.nastav_zdroj(ui.checkBox->isChecked(), ui.spinBox_4->value(), ui.spinBox_5->value(), ui.spinBox_6->value());
	premietni();
}
