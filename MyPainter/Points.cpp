#include "Points.h"
#include <iostream>



Points::Points()
{

}

Points::Points(double X, double Y, double Z) {
	x = X;
	y = Y;
	z = Z;
}

double Points::operator*(Points A) {
	double skalar;
	skalar = A.x*x + A.y*y + A.z*z;
	return(skalar);

}

Points Points::operator+(Points A) {
	Points B;
	B.x = x + A.x;
	B.y = y + A.y;
	B.z = z + A.z;
	return (B);
}

Points Points::operator-(Points A) {
	Points B;
	B.x = x - A.x;
	B.y = y - A.y;
	B.z = z - A.z;
	return(B);
}

Points Points::operator/(Points A) {
	Points vektor;
	double norm;
	vektor.x = y*A.z - z*A.y;
	vektor.y = z*A.x - x*A.z;
	vektor.z = x*A.y - y*A.x;
	return(vektor.norma());
	
}

Points Points::operator*(double t) {
	Points A;
	A.x = t*x;
	A.y = t*y;
	A.z = t*z;
	return (A);
}

Points Points::norma() {
	Points A;
	double norm = x*x + y*y + z*z;
	norm = sqrt(norm);
	A.x= x/ norm;
	A.y=y /norm;
	A.z=z / norm;
	return A;
}

void Points::clear() {
	Nx = 0;
	Ny = 0;
	Nz = 0;
}

void Points::add(Points N) {
	Nx += N.x;
	Ny += N.y;
	Nz += N.z;

}

Points Points::normal() {
	return(Points(Nx,Ny,Nz).norma());
	
}

Points::~Points()
{
}
