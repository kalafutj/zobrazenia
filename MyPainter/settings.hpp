﻿#pragma once
#include <QWidget>
#include "ui_settings.h"
#include "paintWidget.h"

class Settings : public QWidget {
	Q_OBJECT

public:
	Settings(PaintWidget* ,QWidget * parent = Q_NULLPTR);
	~Settings();
public slots:
	//void ok();
	//void notok();

private:
	Ui::Settings ui;
	PaintWidget* u;
};
