#include "priamka.h"



priamka::priamka(Points* A,Points* B)
{
	x = A->x;
	X = B->x;
	z = A->z;
	if (abs(B->y - A->y) < 0.0001) nekon_smer = true;
	k = (B->x - A->x) / (B->y - A->y);
	l = (B->z - A->z) / (B->y - A->y);
}

priamka priamka::operator++(int a) {
	if (nekon_smer) return *this;
	x += k;
	z += l;
	if (k > 0 && x > X)
		x = X;
	if (k < 0 && x < X)
		x = X;
	return *this;
}

bool priamka::operator<(priamka P) {
	if (k < P.k)return true;
	else return false;

}

priamka::~priamka()
{
}
