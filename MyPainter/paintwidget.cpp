#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
	transformovane = new Points[1];
	body = new Points[8];
	T = new Trojuholnik[12];
	Fluc = QColor(0, 0, 255);
	Fokolie = QColor(255, 255, 255);
	zdroj = true;
	luc.x = 0, luc.y = 0, luc.z = 500;
	pociatok.x = 0;pociatok.y = 0;pociatok.z = -500;



/*	body[0].x = -200; body[0].y = -200; body[0].z = -200;
	body[1].x = 200; body[0].y = -200; body[0].z = -200;
	body[2].x = -200; body[0].y = 200; body[0].z = -200;
	body[3].x = -200; body[3].y = -200; body[3].z = 200;
	body[4].x = 200; body[4].y = 200; body[4].z = -200;
	body[5].x = -200; body[5].y = 200; body[5].z = 200;
	body[6].x = 200; body[6].y = -200; body[6].z = 200;
	body[7].x = 200; body[7].y = 200; body[7].z = 200;

	T[0].A = 0;T[0].B = 1;T[0].C = 2;
	T[1].A = 0;T[1].B = 2;T[1].C = 3;
	T[2].A = 0;T[2].B = 3;T[2].C = 1;
	T[3].A = 1;T[3].B = 4;T[3].C = 2;
	T[4].A = 2;T[4].B = 5;T[4].C = 3;
	T[5].A = 3;T[5].B = 6;T[5].C = 1;*/
	
}

bool PaintWidget::openImage(const QString &fileName)
{
	QFile loaded(fileName);
	loaded.open(QIODevice::ReadOnly);

	QString word;
	QStringList Words;
	while (!word.contains("POINTS")) word = loaded.readLine();

	pocet= word.split(" ")[1].toInt();	

	delete[] body;
	body = new Points [pocet];

	for (int i = 0;i < pocet;i++) {
		word = loaded.readLine();
		Words = word.split(" ");
		body[i].x = Words[0].toDouble();
		body[i].y = Words[1].toDouble();
		body[i].z = Words[2].toDouble();
	}

	delete[] transformovane;
	transformovane = new Points[pocet];

	while (!word.contains("POLYGONS")) word = loaded.readLine();
	
	delete[] T;

	POCET = word.split(" ")[1].toInt();
	T = new Trojuholnik[POCET];

	for (int i = 0;i < POCET;i++) {
		word = loaded.readLine();
		Words = word.split(" ");
		T[i].A = transformovane+Words[1].toInt();
		T[i].B = transformovane+Words[2].toInt();
		T[i].C = transformovane+Words[3].toInt();
	}


	loaded.close();

	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	ZZ = new double*[image.width()];
	for (int i = 0;i < image.width();i++) {
		ZZ[i] = new double[image.height()];
		for (int j = 0;j < image.height();j++)
			ZZ[i][j] = MAXINT;
	}
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	int x, y;
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		//painting = true;
		x = lastPoint.x();
		y = lastPoint.y();
		std::cout << x << ',' << y << ',' << ZZ[x][y]<<std::endl;
		std::cout << image.pixelColor(lastPoint).red() << " " << image.pixelColor(lastPoint).green() << " " << image.pixelColor(lastPoint).blue() << std::endl;
		std::cout << std::endl;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	int x, y;
	if ((event->buttons() & Qt::LeftButton) /*&& painting*/)
		//drawLineTo(event->pos());
	{
		lastPoint = event->pos();
		//painting = true;
		x = lastPoint.x();
		y = lastPoint.y();
		std::cout << x << ',' << y << ',' << ZZ[x][y] << std::endl;
		std::cout << image.pixelColor(lastPoint).red() << " " << image.pixelColor(lastPoint).green() << " " << image.pixelColor(lastPoint).blue() << std::endl;
		std::cout << std::endl;
	}

}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::dda(int zx, int zy, int kx, int ky,QColor farba) {


	if (abs(ky - zy) > abs(kx - zx)) {
		double k = (kx - zx) / (double)(ky - zy);
		double q = kx - k*ky;
		if (zy > ky) for (int y = zy; y > ky;y--) if (!mimo(k*y+q, y)) image.setPixelColor(k*y + q, y, farba);
		else for (int y = zy;y < ky;y++) if (!mimo(k*y+q, y)) image.setPixelColor(k*y + q, y, farba);
	}
	else {
		double k = (ky - zy) / (double)(kx - zx);
		double q = ky - k*kx;
		if (zx > kx)for (int x = zx;x > kx;x--) if (!mimo(x, k*x+q)) image.setPixelColor(x, k*x + q, farba);
		else for (int x = zx;x < kx;x++) if (!mimo(x, k*x+q)) image.setPixelColor(x, k*x + q, farba);
	}
}

void PaintWidget::rovnobezne(bool gr) {

	/*double **Z;

	Z = new double*[image.width()];
	for (int i = 0;i < image.width();i++) {
		Z[i] = new double[image.height()];
		for (int j = 0;j < image.height();j++)
			Z[i][j] = INFINITE;
	}*/

	clearImage();

	pociatok = Points(0, 0, 1);

	if (gr) {
		for (int n = 0;n < pocet;n++) transformovane[n].clear();
		
		for (int n = 0;n < POCET;n++) {
			T[n].nastav();
			T[n].A->add(T[n].N);
			T[n].B->add(T[n].N);
			T[n].C->add(T[n].N);
		}
		// for (int n = 0;n < pocet;n++) std::cout << transformovane[n].Nx << ',' << transformovane[n].Ny << ',' << transformovane[n].Nz << std::endl;

		for (int n = 0;n < POCET;n++) PLOCHA(n, ZZ/*Z*/);
	}
	else for (int n = 0;n < POCET;n++) {
		T[n].nastav();
		plocha(n, ZZ);
	}
	
	/*for (int n = 0;n < POCET; n++) {
		dda(400 + T[n].V[0]->x, 400 + T[n].V[0]->y, 400 + T[n].V[1]->x, 400 + T[n].V[1]->y);
		dda(400 + T[n].V[1]->x, 400 + T[n].V[1]->y, 400 + T[n].V[2]->x, 400 + T[n].V[2]->y);
		dda(400 + T[n].V[2]->x, 400 + T[n].V[2]->y, 400 + T[n].V[0]->x, 400 + T[n].V[0]->y);
	}*/
		
	this->update();

/*	for (int i = 0; i < image.width(); i++) 
		delete[] Z[i];
	delete[] Z;*/
}

void PaintWidget::stredove(int stred,bool gr) {

/*	double **Z;

	Z = new double*[image.width()];
	for (int i = 0;i < image.width();i++) {
		Z[i] = new double[image.height()];
		for (int j = 0;j < image.height();j++)
			Z[i][j] = INFINITE;
	}*/

	clearImage();

	for (int n = 0;n < pocet;n++) {
		transformovane[n].x = stred*transformovane[n].x / (stred - transformovane[n].z);
		transformovane[n].y = stred*transformovane[n].y / (stred - transformovane[n].z);
	}

	pociatok = Points(0, 0, -stred);

	if (gr) {
		for (int n = 0; n < pocet; n++) body[n].clear();

		for (int n = 0; n < POCET; n++) {
			T[n].nastav();
			T[n].A->add(T[n].N);
			T[n].B->add(T[n].N);
			T[n].C->add(T[n].N);
		}
		for (int n = 0; n < POCET; n++) PLOCHA(n, ZZ/*Z*/);
	}
	else for (int n = 0;n < POCET;n++) {
		T[n].nastav();
		plocha(n, ZZ);
	}
	/*for (int n=0;n<POCET;n++){
		dda(400 + T[n].A->x, 400 + T[n].A->y, 400 + T[n].B->x, 400 + T[n].B->y);
		dda(400 + T[n].B->x, 400 + T[n].B->y, 400 + T[n].C->x, 400 + T[n].C->y);
		dda(400 + T[n].C->x, 400 + T[n].C->y, 400 + T[n].A->x, 400 + T[n].A->y);
	}*/
		
	/*for (int i = 0; i < image.width(); i++)
		delete[] Z[i];
	delete[] Z;*/

	this->update();
}

void PaintWidget::rotuj(int PHI, int THETA) {
	double phi = -M_PI*PHI / 180;
	double theta = -M_PI*THETA / 180;

	for (int i = 0;i < image.width();i++)
		for (int j = 0;j < image.height();j++)
			ZZ[i][j] = MAXINT;

	for (int i = 0;i < pocet;i++) {
		transformovane[i].x = body[i].x*cos(phi) - body[i].y*sin(phi)*cos(theta)+body[i].z*sin(phi)*sin(theta);
		transformovane[i].y = body[i].x*sin(phi) + body[i].y*cos(phi)*cos(theta) - body[i].z*cos(phi)*sin(theta);
		transformovane[i].z = body[i].y*sin(theta) + body[i].z*cos(theta);
	}

	luc.x = LUC.x*cos(phi) - LUC.y*sin(phi)*cos(theta) + LUC.z*sin(phi)*sin(theta);
	luc.y = LUC.x*sin(phi) + LUC.y*cos(phi)*cos(theta) + LUC.z*cos(phi)*sin(theta);
	luc.z = LUC.y*sin(theta) + LUC.z*cos(theta);

}

void PaintWidget::plocha(int n,double** Z) {
	QColor farba=nater((*T[n].A + *T[n].B + *T[n].C)*(1 / 3.),T[n].N);

	priamka p01(T[n].V[0], T[n].V[1]);
	priamka p02(T[n].V[0], T[n].V[2]);
	priamka p12(T[n].V[1], T[n].V[2]);

	double z, q;

/*	for (int i = 0; i < 3; i++) if (Z[int(400 + T[n].V[i]->x)][400 + int(T[n].V[i]->y)]>T[n].V[i]->z) {
		image.setPixelColor(400+T[n].V[i]->x, 400+T[n].V[i]->y, farba);
		Z[int(400 + T[n].V[i]->x)][400 + int(T[n].V[i]->y)] = T[n].V[i]->z;
	}*/

	if (p01 < p02) {
		for (int y = 400+T[n].V[0]->y;y < 400 +T[n].V[1]->y;++y) {
			if (abs(p02.x - p01.x) < 0.0001) {
				z = p02.z;
				q = 0;
			}
			else {
				q = (p02.z - p01.z) / (p02.x - p01.x);
				z = p01.z;
			}
			for (int x = 399 + p01.x;x < 401 + p02.x;++x) {
				z += q;
				if (mimo(x, y)) continue;
				if (z > Z[x][y]) continue;
				Z[x][y] = z;
				image.setPixelColor(x, y, farba);
			}
			p01++;
			p02++;
		}
		for (int y = 400+T[n].V[1]->y;y < 400+T[n].V[2]->y;++y) {
			if (abs(p02.x - p12.x) < 0.0001) {
				z = p02.z;
				q = 0;
			}
			else {
				q = (p02.z - p12.z) / (p02.x - p12.x);
				z = p12.z;
			}
			for (int x = 399 + p12.x;x < 401 + p02.x;++x) {
				z += q;
				if (mimo(x, y)) continue;
				if (z > Z[x][y]) continue;
				Z[x][y] = z;
				image.setPixelColor(x, y, farba);
			}
			p12++;
			p02++;
		}
	}
	else {
		for (int y = 400 + T[n].V[0]->y;y < 400 + T[n].V[1]->y;++y) {
			if (abs(p01.x - p02.x) < 0.0001) {
				z = p01.z;
				q = 0;
			}
			else {
				q = (p01.z - p02.z) / (p01.x - p02.x);
				z = p02.z;
			}
			for (int x = 399 + p02.x;x < 401 + p01.x;++x){
				z += q;
				if (mimo(x, y)) continue;
				if (z > Z[x][y]) continue;
				Z[x][y] = z;
				image.setPixelColor(x, y, farba);
			}
			p01++;
			p02++;
		}
		for (int y = 400 + T[n].V[1]->y;y < 399 + T[n].V[2]->y;++y) {
			if (abs(p12.x - p02.x) < 0.0001) {
				z = p12.z;
				q = 0;
			}
			else {
				q = (p12.z - p02.z) / (p12.x - p02.x);
				z = p02.z;
			}
			for (int x = 399 + p02.x;x < 401 + p12.x;++x) {
				if (mimo(x, y)) continue;
				if (z > Z[x][y]) continue;
				Z[x][y] = z;
				image.setPixelColor(x, y, farba);
			}
			p12++;
			p02++;
		}
	}

}

void PaintWidget::PLOCHA(int n, double** Z) {
	priamka p01(T[n].V[0], T[n].V[1]);
	priamka p02(T[n].V[0], T[n].V[2]);
	priamka p12(T[n].V[1], T[n].V[2]);

	interpolator mid,beg,end;

	double z, q;

	
	if (p01 < p02) {
		beg.reset(&nater(T[n].V[0]),&nater(T[n].V[1]), 400 + T[n].V[0]->y,400 + T[n].V[1]->y);
		end.reset(&nater(T[n].V[0]), &nater(T[n].V[2]), 400 + T[n].V[0]->y, 400 + T[n].V[2]->y);
		for (int y = 400 + T[n].V[0]->y;y < 400 + T[n].V[1]->y;y++) {
			if (abs(p02.x - p01.x) < 0.0001) {
				z = p02.z;
				q = 0;
			}
			else {
				q = (p02.z - p01.z) / (p02.x - p01.x);
				z = p01.z;
			}
			mid.reset(&beg++, &end++, 399 + p01.x, 401 + p02.x);
			for (int x = 399 + p01.x;x < 401 + p02.x;x++) {
				z += q;
				if (mimo(x, y)) continue;
				if (z > Z[x][y]) continue;
				Z[x][y] = z;
				image.setPixelColor(x, y, mid++);
			}
			p01++;
			p02++;
		}
		beg.reset(&nater(T[n].V[1]), &nater(T[n].V[2]), 400 + T[n].V[1]->y, 400 + T[n].V[2]->y);
		for (int y = 400 + T[n].V[1]->y;y < 400 + T[n].V[2]->y;y++) {
			if (abs(p02.x - p12.x) < 0.0001) {
				z = p02.z;
				q = 0;
			}
			else {
				q = (p02.z - p12.z) / (p02.x - p12.x);
				z = p12.z;
			}
			mid.reset(&beg++, &end++, 399 + p12.x, 401 + p02.x);
			for (int x = 399 + p12.x;x < 401 + p02.x;x++) {
				z += q;
				if (mimo(x, y)) continue;
				if (z > Z[x][y]) continue;
				Z[x][y] = z;
				image.setPixelColor(x, y, mid++);
			}
			p12++;
			p02++;
		}
	}
	else {
		end.reset(&nater(T[n].V[0]), &nater(T[n].V[1]), 400 + T[n].V[0]->y, 400 + T[n].V[1]->y);
		beg.reset(&nater(T[n].V[0]), &nater(T[n].V[2]), 400 + T[n].V[0]->y, 400 + T[n].V[2]->y);
		for (int y = 400 + T[n].V[0]->y;y < 400 + T[n].V[1]->y;y++) {
			if (abs(p01.x - p02.x) < 0.0001) {
				z = p01.z;
				q = 0;
			}
			else {
				q = (p01.z - p02.z) / (p01.x - p02.x);
				z = p02.z;
			}
			mid.reset(&beg++, &end++, 399 + p02.x, 401 + p01.x);
			for (int x = 399 + p02.x;x < 401 + p01.x;x++) {
				z += q;
				if (mimo(x, y)) continue;
				if (z > Z[x][y]) continue;
				Z[x][y] = z;
				image.setPixelColor(x, y, mid++);
			}
			p01++;
			p02++;
		}
		end.reset(&nater(T[n].V[1]), &nater(T[n].V[2]), 400 + T[n].V[1]->y, 400 + T[n].V[2]->y);
		for (int y = 400 + T[n].V[1]->y;y < 400 + T[n].V[2]->y;y++) {
			if (abs(p12.x - p02.x) < 0.0001) {
				z = p12.z;
				q = 0;
			}
			else {
				q = (p12.z - p02.z) / (p12.x - p02.x);
				z = p02.z;
			}
			mid.reset(&beg++, &end++, 399 + p02.x, 401 + p12.x);
			for (int x = 399 + p02.x;x < 401 + p12.x;x++) {
				if (mimo(x, y)) continue;
				if (z > Z[x][y]) continue;
				Z[x][y] = z;
				image.setPixelColor(x, y, mid++);
			}
			p12++;
			p02++;
		}
	}
}

QColor PaintWidget::nater(Points bod, Points N) {
	int F[3];
	double konst;
	Points R;

	F[0] = ra*Fokolie.red();
	F[1] = ra*Fokolie.green();
	F[2] = ra*Fokolie.blue();

	if (zdroj) konst = ((bod - luc).norma()*N)*rd;
	else konst = (luc.norma()*N)*rd;

	if (konst <= 0) return (nafarbi(F));

	F[0] += konst*Fluc.red();
	F[1] += konst*Fluc.green();
	F[2] += konst*Fluc.blue();


	if (zdroj) R = N * 2 * (N*(bod - luc).norma()) - (bod - luc).norma();
	else R = N * 2 * (N*luc.norma()) - luc.norma();

	if (pociatok.z == 1) {
		konst = rs*pow(R.z, h);
		if (R.z <= 0) return(nafarbi(F));
	}
	else {
		konst = rs*pow(((bod - pociatok).norma()*R), h);
		if (((bod - pociatok).norma()*R) <= 0) return(nafarbi(F));
	}


	F[0] += konst*Fluc.red();
	F[1] += konst*Fluc.green();
	F[2] += konst*Fluc.blue();

	return(nafarbi(F));

}

QColor PaintWidget::nater(Points* vrchol) {
	int F[3];
	double konst;
	Points R,N=vrchol->normal();



	F[0] = ra*Fokolie.red();
	F[1] = ra*Fokolie.green();
	F[2] = ra*Fokolie.blue();

	if (zdroj) konst = ((*vrchol - luc).norma()*N)*rd;
	else konst = (luc.norma()*N)*rd;

	if (konst <= 0) return (nafarbi(F));
	F[0] += konst*Fluc.red();
	F[1] += konst*Fluc.green();
	F[2] += konst*Fluc.blue();


	if (zdroj) R = N * 2 * (N*(*vrchol - luc).norma()) - (*vrchol - luc).norma();
	else R = N * 2 * (N*luc.norma()) - luc.norma();

	if (pociatok.z == 1) {
		konst = rs*pow(R.z, h);
		if (R.z <= 0) return(nafarbi(F));
	}
	else {
		konst = rs*pow(((*vrchol - pociatok).norma()*R), h);
		if (((*vrchol - pociatok).norma()*R) <= 0) return(nafarbi(F));
	}


	F[0] += konst*Fluc.red();
	F[1] += konst*Fluc.green();
	F[2] += konst*Fluc.blue();

	return(nafarbi(F));

}

QColor PaintWidget::nafarbi(int* F) {

	if (F[0] < 0) F[0] = 0;
	if (F[0] > 255) F[0] = 255;
	if (F[1] < 0) F[1] = 0;
	if (F[1] > 255) F[1] = 255;
	if (F[2] < 0) F[2] = 0;
	if (F[2] > 255) F[2] = 255;

	return (QColor(F[0], F[1], F[2]));

}

bool PaintWidget::mimo(int x, int y) {
	if (x < 0) return true;
	if (x >= image.width())return true;
	if (y < 0)return true;
	if (y >= image.height())return true;
	return false;
}

void PaintWidget::nastav_zdroj(bool zd,int x,int y,int z) {
	zdroj = zd;
	LUC.x = x;
	LUC.y = y;
	LUC.z = z;
}

void PaintWidget::nastav_farbu(int cl, int zl, int ml, int co, int zo, int mo) {
	Fluc = QColor(cl, zl, ml);
	Fokolie = QColor(co, zo, mo);
}

void PaintWidget::nastav_koeficienty(double a, double s, double d,int o) {
	ra = a;
	rs = s;
	rd = d;
	h = o;
}
