#pragma once
#include <qstring.h>

class Points
{
public:
	Points();
	Points(double, double, double);
	~Points();
	double operator*(Points);
	Points operator-(Points);
	Points operator+(Points);
	Points operator/(Points);
	Points operator*(double);
	Points norma();
	Points normal();
	void clear();
	void add(Points);

	double x, y, z;
	double Nx, Ny, Nz;
};
