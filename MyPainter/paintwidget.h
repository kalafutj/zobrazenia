#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <iostream>
#include "Points.h"
#include <QStringList>
#include "Trojuholnik.h"
#include "priamka.h"
#include "interpolator.h"

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void rovnobezne(bool);
	void stredove(int,bool);
	void rotuj(int, int);
	void plocha(int,double**);
	void PLOCHA(int, double**);
	void nastav_zdroj(bool,int,int,int);
	void nastav_farbu(int,int,int,int,int,int);
	void nastav_koeficienty(double, double, double,int);

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	void dda(int,int,int,int,QColor);
//	QColor nater(int);
	QColor nater(Points, Points);
	QColor nater(Points*);
	bool mimo(int, int);
	QColor nafarbi(int*);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	double phi, theta;
	Points* body;
	int pocet;
	Points* transformovane;
	int POCET;
	Trojuholnik* T;
	bool zdroj=true;
	double rs=0,rd=1,ra=0.25;
	Points luc,LUC,pociatok;
	QColor Fluc, Fokolie;
	int h=1;
	double** ZZ;

};

#endif // PAINTWIDGET_H
