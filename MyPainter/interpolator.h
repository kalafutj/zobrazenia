#pragma once
#include "Points.h"
#include <QColor>

class interpolator
{
public:
	interpolator();
	interpolator(QColor*, QColor*,double,double);
	~interpolator();
	void reset(QColor*, QColor*, double, double);
	QColor operator++(int);
	
private:
	double kr, kg, kb;
	double r, g, b;
	int R, G, B;

};
