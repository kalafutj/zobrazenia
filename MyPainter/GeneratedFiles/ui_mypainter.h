/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QAction *actionClear_2;
    QAction *actionSet_Options;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QHBoxLayout *horizontalLayout_8;
    QFormLayout *formLayout;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_4;
    QSpinBox *spinBox_4;
    QSpinBox *spinBox_5;
    QSpinBox *spinBox_6;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_5;
    QSpinBox *spinBox_7;
    QSpinBox *spinBox_8;
    QSpinBox *spinBox_9;
    QLabel *label_6;
    QHBoxLayout *horizontalLayout_6;
    QHBoxLayout *horizontalLayout_7;
    QSpinBox *spinBox_10;
    QSpinBox *spinBox_11;
    QSpinBox *spinBox_12;
    QLabel *label_7;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_8;
    QDoubleSpinBox *doubleSpinBox_2;
    QLabel *label_9;
    QDoubleSpinBox *doubleSpinBox_3;
    QCheckBox *checkBox;
    QPushButton *pushButton;
    QLabel *label_10;
    QSpinBox *spinBox_13;
    QCheckBox *checkBox_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QSpinBox *spinBox;
    QLabel *label_2;
    QSpinBox *spinBox_2;
    QLabel *label_3;
    QSpinBox *spinBox_3;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEffects;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(626, 387);
        MyPainterClass->setMinimumSize(QSize(500, 0));
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        actionClear_2 = new QAction(MyPainterClass);
        actionClear_2->setObjectName(QStringLiteral("actionClear_2"));
        actionSet_Options = new QAction(MyPainterClass);
        actionSet_Options->setObjectName(QStringLiteral("actionSet_Options"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setEnabled(true);
        scrollArea->setMinimumSize(QSize(100, 0));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 297, 269));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        horizontalLayout->addWidget(scrollArea);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setSizeConstraint(QLayout::SetFixedSize);
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
        formLayout->setContentsMargins(0, -1, -1, -1);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetFixedSize);
        spinBox_4 = new QSpinBox(centralWidget);
        spinBox_4->setObjectName(QStringLiteral("spinBox_4"));
        spinBox_4->setMinimum(-10000);
        spinBox_4->setMaximum(10000);

        horizontalLayout_4->addWidget(spinBox_4);

        spinBox_5 = new QSpinBox(centralWidget);
        spinBox_5->setObjectName(QStringLiteral("spinBox_5"));
        spinBox_5->setMinimum(-10000);
        spinBox_5->setMaximum(10000);

        horizontalLayout_4->addWidget(spinBox_5);

        spinBox_6 = new QSpinBox(centralWidget);
        spinBox_6->setObjectName(QStringLiteral("spinBox_6"));
        spinBox_6->setMinimum(-10000);
        spinBox_6->setMaximum(10000);
        spinBox_6->setValue(1000);

        horizontalLayout_4->addWidget(spinBox_6);


        formLayout->setLayout(3, QFormLayout::FieldRole, horizontalLayout_4);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setSizeConstraint(QLayout::SetFixedSize);
        spinBox_7 = new QSpinBox(centralWidget);
        spinBox_7->setObjectName(QStringLiteral("spinBox_7"));
        spinBox_7->setMaximum(255);
        spinBox_7->setValue(255);

        horizontalLayout_5->addWidget(spinBox_7);

        spinBox_8 = new QSpinBox(centralWidget);
        spinBox_8->setObjectName(QStringLiteral("spinBox_8"));
        spinBox_8->setMaximum(255);

        horizontalLayout_5->addWidget(spinBox_8);

        spinBox_9 = new QSpinBox(centralWidget);
        spinBox_9->setObjectName(QStringLiteral("spinBox_9"));
        spinBox_9->setMaximum(255);

        horizontalLayout_5->addWidget(spinBox_9);


        formLayout->setLayout(4, QFormLayout::FieldRole, horizontalLayout_5);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        spinBox_10 = new QSpinBox(centralWidget);
        spinBox_10->setObjectName(QStringLiteral("spinBox_10"));
        spinBox_10->setMaximum(255);
        spinBox_10->setValue(255);

        horizontalLayout_7->addWidget(spinBox_10);

        spinBox_11 = new QSpinBox(centralWidget);
        spinBox_11->setObjectName(QStringLiteral("spinBox_11"));
        spinBox_11->setMaximum(255);
        spinBox_11->setValue(255);

        horizontalLayout_7->addWidget(spinBox_11);

        spinBox_12 = new QSpinBox(centralWidget);
        spinBox_12->setObjectName(QStringLiteral("spinBox_12"));
        spinBox_12->setMaximum(255);
        spinBox_12->setValue(255);

        horizontalLayout_7->addWidget(spinBox_12);


        horizontalLayout_6->addLayout(horizontalLayout_7);


        formLayout->setLayout(5, QFormLayout::FieldRole, horizontalLayout_6);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(7, QFormLayout::LabelRole, label_7);

        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setDecimals(4);
        doubleSpinBox->setMaximum(1);
        doubleSpinBox->setSingleStep(0.01);
        doubleSpinBox->setValue(0.1);

        formLayout->setWidget(7, QFormLayout::FieldRole, doubleSpinBox);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_8);

        doubleSpinBox_2 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setDecimals(4);
        doubleSpinBox_2->setMaximum(1);
        doubleSpinBox_2->setSingleStep(0.01);
        doubleSpinBox_2->setValue(0.5);

        formLayout->setWidget(8, QFormLayout::FieldRole, doubleSpinBox_2);

        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout->setWidget(9, QFormLayout::LabelRole, label_9);

        doubleSpinBox_3 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setDecimals(4);
        doubleSpinBox_3->setMaximum(1);
        doubleSpinBox_3->setSingleStep(0.01);
        doubleSpinBox_3->setValue(0.1);

        formLayout->setWidget(9, QFormLayout::FieldRole, doubleSpinBox_3);

        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QStringLiteral("checkBox"));

        formLayout->setWidget(1, QFormLayout::FieldRole, checkBox);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout->setWidget(11, QFormLayout::LabelRole, pushButton);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout->setWidget(10, QFormLayout::LabelRole, label_10);

        spinBox_13 = new QSpinBox(centralWidget);
        spinBox_13->setObjectName(QStringLiteral("spinBox_13"));
        spinBox_13->setMinimum(1);
        spinBox_13->setMaximum(99);

        formLayout->setWidget(10, QFormLayout::FieldRole, spinBox_13);

        checkBox_2 = new QCheckBox(centralWidget);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));

        formLayout->setWidget(11, QFormLayout::FieldRole, checkBox_2);


        horizontalLayout_8->addLayout(formLayout);


        horizontalLayout->addLayout(horizontalLayout_8);


        verticalLayout->addLayout(horizontalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, -1, -1, -1);
        radioButton = new QRadioButton(centralWidget);
        radioButton->setObjectName(QStringLiteral("radioButton"));

        horizontalLayout_2->addWidget(radioButton);

        radioButton_2 = new QRadioButton(centralWidget);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));

        horizontalLayout_2->addWidget(radioButton_2);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_3->addWidget(label);

        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMinimum(-180);
        spinBox->setMaximum(180);

        horizontalLayout_3->addWidget(spinBox);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        spinBox_2 = new QSpinBox(centralWidget);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setMinimum(-180);
        spinBox_2->setMaximum(180);

        horizontalLayout_3->addWidget(spinBox_2);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_3->addWidget(label_3);

        spinBox_3 = new QSpinBox(centralWidget);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setMinimum(100);
        spinBox_3->setMaximum(1000);
        spinBox_3->setValue(500);

        horizontalLayout_3->addWidget(spinBox_3);


        verticalLayout_2->addLayout(horizontalLayout_3);


        verticalLayout->addLayout(verticalLayout_2);

        MyPainterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 626, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEffects = new QMenu(menuBar);
        menuEffects->setObjectName(QStringLiteral("menuEffects"));
        MyPainterClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEffects->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuEffects->addAction(actionClear_2);

        retranslateUi(MyPainterClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), MyPainterClass, SLOT(ActionOpen()));
        QObject::connect(actionSave, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSave()));
        QObject::connect(actionClear, SIGNAL(triggered()), MyPainterClass, SLOT(EffectClear()));
        QObject::connect(actionNew, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNew()));
        QObject::connect(spinBox_2, SIGNAL(valueChanged(int)), MyPainterClass, SLOT(premietni()));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), MyPainterClass, SLOT(premietni()));
        QObject::connect(radioButton, SIGNAL(clicked()), MyPainterClass, SLOT(premietni()));
        QObject::connect(radioButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(premietni()));
        QObject::connect(pushButton, SIGNAL(clicked()), MyPainterClass, SLOT(options()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", 0));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", 0));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", 0));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", 0));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", 0));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", 0));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", 0));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", 0));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", 0));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", 0));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", 0));
        actionClear_2->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        actionSet_Options->setText(QApplication::translate("MyPainterClass", "Set Options", 0));
        label_4->setText(QApplication::translate("MyPainterClass", "Poloha zdroja svetla", 0));
        label_5->setText(QApplication::translate("MyPainterClass", "Farba svetelneho luca", 0));
        label_6->setText(QApplication::translate("MyPainterClass", "Farba okolia", 0));
        label_7->setText(QApplication::translate("MyPainterClass", "Koeficient odrazu", 0));
        label_8->setText(QApplication::translate("MyPainterClass", "Koeficient difuzie", 0));
        label_9->setText(QApplication::translate("MyPainterClass", "Ambientny koeficient", 0));
        checkBox->setText(QApplication::translate("MyPainterClass", "Bodov zdroj (inak plosny)", 0));
        pushButton->setText(QApplication::translate("MyPainterClass", "OK", 0));
        label_10->setText(QApplication::translate("MyPainterClass", "Ostrost", 0));
        checkBox_2->setText(QApplication::translate("MyPainterClass", "Goraudovo tienovanie", 0));
        radioButton->setText(QApplication::translate("MyPainterClass", "Rovnobezne premietanie", 0));
        radioButton_2->setText(QApplication::translate("MyPainterClass", "Stredove premitanie", 0));
        label->setText(QApplication::translate("MyPainterClass", "Rho", 0));
        label_2->setText(QApplication::translate("MyPainterClass", "Phi", 0));
        label_3->setText(QApplication::translate("MyPainterClass", "Vzdialenost stredu", 0));
        menuFile->setTitle(QApplication::translate("MyPainterClass", "File", 0));
        menuEffects->setTitle(QApplication::translate("MyPainterClass", "Effects", 0));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
